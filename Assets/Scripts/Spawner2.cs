﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Spawner2 : MonoBehaviour
{
    //Enemy[] enemy = FindObjectsOfType(typeof(Enemy)) as Enemy[];
    public Text timer;
    public Text scoresText;
    public GameObject Enemy1, Enemy2, Enemy3, nextwave, spawner, Teleport1, Base, Base1, compl, StartWave;
    [SerializeField] private int dead, scan, k, spawnmob, wave = 1;
    public float SpawnTimer = 10f, sec;
    public Transform[] spawnPos;
    //private string Id;
    //private string n;

    void Start()
    {
        sec = 3f;
        PlayerPrefs.SetInt("kolvo", k);
        dead = 0;
        PlayerPrefs.SetInt("Dead", dead);
        StartCoroutine(SpawnCD());
    }
    void Update()
    {
        k = PlayerPrefs.GetInt("kolvo", k);
        dead = PlayerPrefs.GetInt("Dead", dead);
        scoresText.text = "Wave:  " + wave;
        timer.text = "in  " + sec;
    }
    void Repeat()
    {
        StartCoroutine(SpawnCD());
    }
    IEnumerator SpawnCD()
    {
        yield return new WaitForSeconds(SpawnTimer);
        if (wave == 1 && k == 0)
        {
            StartWave.SetActive(true);
            yield return new WaitForSeconds(1);
            sec--;
            yield return new WaitForSeconds(1);
            sec--;
            yield return new WaitForSeconds(1);
            StartWave.SetActive(false);
            sec = 3;

            yield return new WaitForSeconds(0.5f);
            nextwave.SetActive(true);
            yield return new WaitForSeconds(2);
            nextwave.SetActive(false);
        }
        switch (wave)
        {
            case 1:
                spawnmob = 8;
                if (dead < spawnmob)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        int number1 = Random.Range(0, 8);
                        int Enemyn = Random.Range(1, 4);
                        if (Enemyn == 1)
                            Instantiate(Enemy1, spawnPos[number1].position, Quaternion.identity);
                        if (Enemyn == 2)
                            Instantiate(Enemy2, spawnPos[number1].position, Quaternion.identity);
                        if (Enemyn == 3)
                            Instantiate(Enemy3, spawnPos[number1].position, Quaternion.identity);
                        yield return new WaitForSeconds(0.3f);
                    }
                }

                if (dead >= spawnmob)
                {
                    do
                    {
                        yield return new WaitForSeconds(2f);

                    } while (dead != k);

                    wave++;

                    nextwave.SetActive(true);
                    yield return new WaitForSeconds(2);
                    nextwave.SetActive(false);

                    k = 0;
                    PlayerPrefs.SetInt("kolvo", k);
                    dead = 0;
                    PlayerPrefs.SetInt("Dead", dead);
                    spawnmob = 12;


                    break;
                }

                break;
            case 2:

                if (dead < spawnmob)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        int number1 = Random.Range(0, 8);
                        int Enemyn = Random.Range(1, 4);
                        if (Enemyn == 1)
                            Instantiate(Enemy1, spawnPos[number1].position, Quaternion.identity);
                        if (Enemyn == 2)
                            Instantiate(Enemy2, spawnPos[number1].position, Quaternion.identity);
                        if (Enemyn == 3)
                            Instantiate(Enemy3, spawnPos[number1].position, Quaternion.identity);
                        yield return new WaitForSeconds(0.3f);
                    }
                }

                if (dead >= spawnmob)
                {
                    do
                    {
                        yield return new WaitForSeconds(2f);

                    } while (dead != k);

                    wave++;

                    nextwave.SetActive(true);
                    yield return new WaitForSeconds(2);
                    nextwave.SetActive(false);
                    k = 0;
                    PlayerPrefs.SetInt("kolvo", k);
                    dead = 0;
                    PlayerPrefs.SetInt("Dead", dead);
                    spawnmob = 16;
                }

                break;

            case 3:

                if (dead < spawnmob)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        int number1 = Random.Range(0, 8);
                        int Enemyn = Random.Range(1, 4);
                        if (Enemyn == 1)
                            Instantiate(Enemy1, spawnPos[number1].position, Quaternion.identity);
                        if (Enemyn == 2)
                            Instantiate(Enemy2, spawnPos[number1].position, Quaternion.identity);
                        if (Enemyn == 3)
                            Instantiate(Enemy3, spawnPos[number1].position, Quaternion.identity);
                        yield return new WaitForSeconds(0.3f);
                    }
                }

                if (dead >= spawnmob)
                {
                    do
                    {
                        yield return new WaitForSeconds(2f);

                    } while (dead != k);

                    wave++;

                    nextwave.SetActive(true);
                    yield return new WaitForSeconds(2);
                    nextwave.SetActive(false);
                    k = 0;
                    PlayerPrefs.SetInt("kolvo", k);
                    dead = 0;
                    PlayerPrefs.SetInt("Dead", dead);
                    spawnmob = 18;
                }

                break;

            case 4:

                if (dead < spawnmob)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        int number1 = Random.Range(0, 8);
                        int Enemyn = Random.Range(1, 4);
                        if (Enemyn == 1)
                            Instantiate(Enemy1, spawnPos[number1].position, Quaternion.identity);
                        if (Enemyn == 2)
                            Instantiate(Enemy2, spawnPos[number1].position, Quaternion.identity);
                        if (Enemyn == 3)
                            Instantiate(Enemy3, spawnPos[number1].position, Quaternion.identity);
                        yield return new WaitForSeconds(0.3f);
                    }
                }

                if (dead >= spawnmob)
                {
                    do
                    {
                        yield return new WaitForSeconds(2f);

                    } while (dead != k);

                    wave++;

                    k = 0;
                    PlayerPrefs.SetInt("kolvo", k);
                    dead = 0;
                    PlayerPrefs.SetInt("Dead", dead);

                    compl.SetActive(true);
                    yield return new WaitForSeconds(2);
                    compl.SetActive(false);

                    Teleport1.SetActive(true);
                    StopCoroutine(SpawnCD());
                    break;
                }
                break;
        }
        /*foreach (Transform go in spawnPos)
        {
            Debug.Log(go.transform.position.x);
            k++;
            n = k.ToString();
            Id = "EnemyPos" + n + "X";

            PlayerPrefs.SetFloat(Id, go.GetInstanceID());
        }*/
        if (wave < 5)
        {
            Repeat();
        }
        else
        {
            wave = 0;
            Base.SetActive(true);
            Base1.SetActive(false);
            spawner.SetActive(false);
        }

    }
}